package com.epam.DOM;

public class Employee {
    private String firstName;
    private String lastName;
    private String email;
    private String dep;

    public String getDep() {
        return dep;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Name: " + getFirstName() + " " + getLastName() +
                " email is : " + getEmail() + " | DEP | " + getDep();
    }
}