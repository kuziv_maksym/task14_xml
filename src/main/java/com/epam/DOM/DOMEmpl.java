package com.epam.DOM;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class DOMEmpl {

    private static Logger logger = LogManager.getRootLogger();
    private static ArrayList<Employee> employeess = new ArrayList<Employee>();
    private static Document document = null;
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        File file = new File("F:\\Epam\\javaxml\\src\\main\\resources\\xml\\file.xml");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        try {
            document = documentBuilder.parse(file);
        } catch (NullPointerException e) {
            e.printStackTrace();
            logger.error("Problem with document | null pointer | ");

        }

        Element employeeraElement = (Element) document.getElementsByTagName("employees").item(0);

        NodeList employeeNodeList = document.getElementsByTagName("employee");

        List<Employee> employeeList = new ArrayList<>();

        for (int i = 0; i < employeeNodeList.getLength(); i++) {
            if (employeeNodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element employeeElement = (Element) employeeNodeList.item(i);
                Employee employee = new Employee();
                NodeList childNode = employeeElement.getChildNodes();
                for (int j = 0; j < childNode.getLength(); j++) {
                    if (childNode.item(j).getNodeType()==Node.ELEMENT_NODE) {
                        Element childElement = (Element) childNode.item(j);
                        switch (childElement.getTagName()) {
                            case "firstname" :{
                                employee.setFirstName(childElement.getTextContent());
                            } break;
                            case "lastname" :{
                                employee.setLastName(childElement.getTextContent());
                            } break;
                            case "email" :{
                                employee.setEmail(childElement.getTextContent());
                            } break;
                            case "department":{
                                employee.setDep(childElement.getTextContent());
                            }
                        }
                    }
                }
                employeeList.add(employee);
            }

        }
        employeeList.forEach(System.out::println);
    }
}
